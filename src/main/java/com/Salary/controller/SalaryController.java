package com.Salary.controller;

import com.Salary.model.Salary;
import com.Salary.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SalaryController {
    @Autowired
    SalaryService salaryService;

    @GetMapping("/get/{age}")
    public long getSalaryByAge(@PathVariable int age){
        return salaryService.getSalaryByAge(age).getSalary();
    }

    @PostMapping("/addData")
    public String addData(@RequestBody List<Salary> salaries){
        return salaryService.addMultiple(salaries);

    }
}
