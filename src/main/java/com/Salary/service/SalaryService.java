package com.Salary.service;

import com.Salary.model.Salary;
import com.Salary.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SalaryService {
    @Autowired
    SalaryRepository salaryRepository;

    public Salary getSalaryByAge(int age){
        Salary salary = salaryRepository.getSalaryByAge(age);
        return salary;
    }

    public String saveSalary(Salary salary){

        salaryRepository.save(salary);
        return "Salary data saved";

    }

    public String addMultiple(List<Salary> salaries){
        List<String> salary= new ArrayList<>();
        for(Salary E: salaries){
            salary.add(saveSalary(E));
        }
        return "added";

    }

}
